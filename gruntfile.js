
module.exports = function (grunt) {
    grunt.initConfig({
        critical: {
            dist: {
                options: {
                    base: './'
                },
                // The source file
                src: 'build/home.html',
                // The destination file
                dest: 'build/home-result1.html'
            }
        }
    });
    // Load the plugins
    grunt.loadNpmTasks('grunt-critical');

    // Default tasks.
    grunt.registerTask('default', ['critical']);
};