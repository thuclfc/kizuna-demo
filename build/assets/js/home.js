/*!
 * kizuna
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 */$(document).ready(function () {
    $('.project_img').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        autoplay: true,
        dots: false,
        arrows: false
    });
    $('.project_list').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        draggable: false,
        swipe: false,
        fade: true,
        autoplaySpeed: 5000,
        autoplay: true,
        centerPadding: 0,
        dots: true,
        arrows: true
    });
    $('.feedback_list').slick({
        centerMode: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: true,
        arrows: false,
        responsive: [{
            breakpoint: 640,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    $('.partner_list').slick({
        centerMode: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: false,
        arrows: false,
        responsive: [{
            breakpoint: 640,
            settings: {
                slidesToShow: 4
            }
        }]
    });

    // counter
    function counter() {
        $('.counter').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({ countNum: $this.text() }).animate({
                countNum: countTo
            }, {
                duration: 3000,
                easing: 'linear',
                step: function () {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function () {
                    $this.text(this.countNum);
                    //alert('finished');
                }
            });
        });
    }

    var runCounter = true;
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var page_scroll = $('.data').offset().top;
        if (page_scroll < window.innerHeight + scroll && runCounter) {
            counter();
            runCounter = false;
        }
    });
});